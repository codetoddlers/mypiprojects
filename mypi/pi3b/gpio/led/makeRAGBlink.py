"""
Requirement : To make LED blink every 2 seconds
Design : setMode to BCM, set GPIO PIN 17,27,22 to OUT
Implementation : as in source code

Hardware: RPi3B J8 Header, Bread Board
Example connection :
Start : 9(GPIO17) > {Jumper RPi to BB}
cont : [R > +LED > -LED] {Jumper BB to RPi}
End :  6(GND)
Caution : resistor is a must, loose connection of jumper wire wastes time
"""


def setInputPin(inputPIN):
    GPIO.setup(inputPIN, GPIO.OUT)

    GPIO.output(inputPIN, GPIO.HIGH)
    print("If LED glows ... ON is working")

    time.sleep(3)

    GPIO.output(inputPIN, GPIO.LOW)
    print("If LED dims ... OFF is working")


def makeLEDBlink(inputPIN):
    iterateOn = 0
    print("Operating on inputPIN ", inputPIN)
    print("LED should start blinking now :-O")
    iterateTill = 10
    blinkEveryNsec = 2
    while (iterateOn < iterateTill):
        if iterateOn % 2 == 0:
            GPIO.output(inputPIN, GPIO.LOW)
        else:
            GPIO.output(inputPIN, GPIO.HIGH)
        time.sleep(blinkEveryNsec)
        iterateOn = iterateOn + 1
        if iterateOn % 5 == 0:
            print("blink counter ", iterateOn)
    print("LED blink stops :)")


import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

setInputPin(17)
makeLEDBlink(17)

setInputPin(27)
makeLEDBlink(27)

setInputPin(22)
makeLEDBlink(22)
