"""
Requirement : To make LED blink every 2 seconds
Design : setMode to BCM, set GPIO PIN 17 to OUT
Implementation : as in source code

Hardware: RPi3B J8 Header, Bread Board
11(GPIO17) > {Jumper RPi to BB}
[R > +LED > -LED] {Jumper BB to RPi}
> 6(GND)
Caution : resistor is a must, loose connection of jumper wire wastes time
"""
gpioInputPin = 17
blinkEveryNsec = 2


def makeLEDBlink():
    iterateOn = 0
    print("gpioInputPin : ", gpioInputPin)
    print("blinkEveryNsec : ", blinkEveryNsec)
    print("LED should start blinking now :-O")
    iterateTill = 10
    while (iterateOn < iterateTill):
        if iterateOn % 2 == 0:
            GPIO.output(gpioInputPin, GPIO.LOW)
        else:
            GPIO.output(gpioInputPin, GPIO.HIGH)
        time.sleep(blinkEveryNsec)
        iterateOn = iterateOn + 1
        if iterateOn % 5 == 0:
            print("blink counter ", iterateOn)
    print("LED blinking stops :)")


import RPi.GPIO as GPIO
import time

print("input PIN is : ", gpioInputPin, ", blink interval is: ", blinkEveryNsec)
GPIO.setmode(GPIO.BCM)
GPIO.setup(gpioInputPin, GPIO.OUT)

GPIO.output(gpioInputPin, GPIO.HIGH)
print("If LED glows ... ON is working")

time.sleep(3)

GPIO.output(gpioInputPin, GPIO.LOW)
print("If LED dims ... OFF is working")

makeLEDBlink()
GPIO.cleanup()
