**Check PI version and GPIO PIN layout**
 $ pinout
        ,--------------------------------.
        | oooooooooooooooooooo J8     +====
        | 1ooooooooooooooooooo        | USB
        |                             +====
        |      Pi Model 3B V1.2          |
        |      +----+                 +====
        | |D|  |SoC |                 | USB
        | |S|  |    |                 +====
        | |I|  +----+                    |
        |                   |C|     +======
        |                   |S|     |   Net
        | pwr        |HDMI| |I||A|  +======
        `-| |--------|    |----|V|-------'

Revision           : a02082
SoC                : BCM2837
RAM                : 1024Mb
Storage            : MicroSD
USB ports          : 4 (excluding power)
Ethernet ports     : 1
Wi-fi              : True
Bluetooth          : True
Camera ports (CSI) : 1
Display ports (DSI): 1

J8:
   3V3  (1) (2)  5V
 GPIO2  (3) (4)  5V
 GPIO3  (5) (6)  GND
 GPIO4  (7) (8)  GPIO14
   GND  (9) (10) GPIO15
GPIO17 (11) (12) GPIO18
GPIO27 (13) (14) GND
GPIO22 (15) (16) GPIO23
   3V3 (17) (18) GPIO24
GPIO10 (19) (20) GND
 GPIO9 (21) (22) GPIO25
GPIO11 (23) (24) GPIO8
   GND (25) (26) GPIO7
 GPIO0 (27) (28) GPIO1
 GPIO5 (29) (30) GND
 GPIO6 (31) (32) GPIO12
GPIO13 (33) (34) GND
GPIO19 (35) (36) GPIO16
GPIO26 (37) (38) GPIO20
   GND (39) (40) GPIO21

For further information, please refer to https://pinout.xyz/
**Check GPIO PIN burn**
How to test if GPIO are not fried ?
        http://abyz.me.uk/rpi/pigpio/
        https://github.com/joan2937/pigpio
        http://abyz.me.uk/rpi/pigpio/examples.html

sudo pigpiod
./gpiotest
This program checks the Pi's (user) gpios.

The program reads and writes all the gpios.  Make sure NOTHING
is connected to the gpios during this test.

The program uses the pigpio daemon which must be running.

To start the daemon use the command sudo pigpiod.

Press the ENTER key to continue or ctrl-C to abort...

Testing...
Skipped non-user gpios: 0 1 28 29 30 31
Tested user gpios: 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
Failed user gpios: None
**Embedded linux**
        https://elinux.org
        https://elinux.org/RPi_Hub